Confounded!
===========

This is my Foundation 4 template. [Foundation](http://foundation.zurb.com) is an HTML/CSS/JS frontend similar to [Twitter Bootstrap](http://getbootstrap.com/). It works with Sass, Compass, and Ruby on Rails.